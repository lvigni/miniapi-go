package main
import (
 "fmt"
 "net/http"
 "time"
 "os"
 "bufio"
)

func getTimeHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
		case http.MethodGet:
			t := time.Now()
			fmt.Fprintf(w, fmt.Sprintf("%02dh%02d", t.Hour(), t.Minute()))
	
	}
}

func postInfoHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
		case http.MethodPost:
			if err := req.ParseForm(); err != nil {
				fmt.Println("Something went bad")
				fmt.Fprintln(w, "Something went bad")
				return
			}
			author := req.PostForm.Get("author")
			entry:= req.PostForm.Get("entry")
			
			resp := author + ":" + entry
					
			saveFile, err := os.OpenFile("./save.data", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
			defer saveFile.Close()

			writter := bufio.NewWriter(saveFile)

			if err == nil {
				fmt.Fprintf(writter, "%s\n", entry)
			}
			writter.Flush()

			fmt.Fprintf(w, resp)
	}
}

func getEntriesHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
		case http.MethodGet:
			saveData, err := os.ReadFile("./save.data")
			if err == nil {
				fmt.Fprintf(w, string(saveData))
			}
	}
}


func main() {
	http.HandleFunc("/", getTimeHandler)
	http.HandleFunc("/add", postInfoHandler)
	http.HandleFunc("/entries", getEntriesHandler)

	http.ListenAndServe(":4567", nil)
}